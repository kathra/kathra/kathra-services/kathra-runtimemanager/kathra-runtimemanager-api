# Kathra Runtime Manager API

This API provides management of the Kathra components into runtime environment. 
Main features insured by RuntimeManager are : 

 - Manage isolated runtime enviromnents 
 - Install and uninstall application from service's catalog [See CatalogManager specs 1.1.x] into runtime enviroments
 - Manage OCI Container
 - Manage storage volumes
 - Manage jobs
 - Extract logs from applications runtimes
 - Expose metrics : system, application
 - Expose applications deployed through HTTP load balancer
 - Backup and restore enviromnents


To consult OpenApi specs, 
import specs from https://gitlab.com/kathra/kathra/kathra-services/kathra-runtimemanager/kathra-runtimemanager-api/raw/master/swagger.yaml into https://editor.swagger.io/
